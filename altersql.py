import settings
from django.core.management import setup_environ
setup_environ(settings)

from django.core.management.sql import *

import sys
sys.modules["django.core.management.sql"] = sys.modules["django_snippets.altersql"]


#import os
#import re
#
#from django.conf import settings
#from django.core.management.base import CommandError
#from django.db import models
#from django.db.models import get_models

def sql_indexes(app, style, connection):
    "Returns a list of the CREATE INDEX SQL statements for all models in the given app."
    output = []

#    connection.creation.sql_indexes_for_model__old = connection.creation.sql_indexes_for_model
#    connection.creation.sql_indexes_for_model = types.MethodType(sql_indexes_for_model__custom, connection.creation)
    
    for model in models.get_models(app):
        output.extend(connection.creation.sql_indexes_for_model(model, style))
    return output



def sql_all(app, style, connection):
    "Returns a list of CREATE TABLE SQL, initial-data inserts, and CREATE INDEX SQL for the given module."
    print '--- custom sql_all ---'
    return sql_create(app, style, connection) + sql_custom(app, style, connection) + sql_indexes(app, style, connection)

def safe_add_column(qn, table, column, type):
#    s = """
#        
#DELIMITER $$
#
#DROP PROCEDURE IF EXISTS upgrade_database_1_0_to_2_0 $$
#CREATE PROCEDURE upgrade_database_1_0_to_2_0()
#BEGIN
#
#    IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
#            AND COLUMN_NAME='%s' AND TABLE_NAME='%s') ) THEN
#        ALTER TABLE %s ADD %s %s;
#    END IF;
#
#END $$
#
#CALL upgrade_database_1_0_to_2_0() $$
#
#DELIMITER ;
#        """ % (column, table, qn(table), qn(column), type)
##    return s.strip().split('\n') 
#    return s 

#    s = [
#        
#        
#        "DROP PROCEDURE IF EXISTS upgrade_database_1_0_to_2_0;",
##        """DELIMITER $$""",
#        """
#        CREATE PROCEDURE upgrade_database_1_0_to_2_0()
#        BEGIN
#        
#            IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
#                    AND COLUMN_NAME='%s' AND TABLE_NAME='%s') ) THEN
#                ALTER TABLE %s ADD %s %s;
#            END IF;
#        
#        END;""" % (column, table, qn(table), qn(column), type),
##        """DELIMITER ;""", 
#    
#        "CALL upgrade_database_1_0_to_2_0();",
#        
#    ]
#        
    
    type_mod = type.replace('UNIQUE', '').replace('PRIMARY KEY', '')
    type_mod.replace
   
    s = [
        """ALTER TABLE %s ADD COLUMN %s %s;""" % (qn(table), qn(column), type),        
        """ALTER TABLE %s MODIFY COLUMN %s %s;""" % (qn(table), qn(column), type_mod),        
#        """ALTER TABLE %s CHANGE COLUMN %s %s %s;""" % tuple([qn(table)] + [qn(column)]*2 + [type]),         
    ]    
    
    return s 
#    return ['\n'.join(s)] 


def sql_create_model__custom(self, model, style, known_models=set()):
    """
    Returns the SQL required to create a single model, as a tuple of:
        (list_of_sql, pending_references_dict)
    """
    
#    print 'sql_create_model__custom'
    
    opts = model._meta
    if not opts.managed or opts.proxy:
        return [], {}
    final_output = []
    table_output = []
    table_output_dict = {}
    pending_references = {}
    qn = self.connection.ops.quote_name
    for f in opts.local_fields:
        col_type = f.db_type(connection=self.connection)
        tablespace = f.db_tablespace or opts.db_tablespace
        if col_type is None:
            # Skip ManyToManyFields, because they're not represented as
            # database columns in this table.
            continue
        # Make the definition (e.g. 'foo VARCHAR(30)') for this field.
        field_output = [style.SQL_FIELD(qn(f.column)),
            style.SQL_COLTYPE(col_type)]
        if not f.null:
            field_output.append(style.SQL_KEYWORD('NOT NULL'))
        if f.primary_key:
            field_output.append(style.SQL_KEYWORD('PRIMARY KEY'))
        elif f.unique:
            field_output.append(style.SQL_KEYWORD('UNIQUE'))
        if tablespace and f.unique:
            # We must specify the index tablespace inline, because we
            # won't be generating a CREATE INDEX statement for this field.
            field_output.append(self.connection.ops.tablespace_sql(tablespace, inline=True))
        if f.rel:
            ref_output, pending = self.sql_for_inline_foreign_key_references(f, known_models, style)
            if pending:
                pr = pending_references.setdefault(f.rel.to, []).append((model, f))
            else:
                field_output.extend(ref_output)
        table_output_dict[f.column] = field_output[1:]
        table_output.append(' '.join(field_output))
    for field_constraints in opts.unique_together:
        table_output.append(style.SQL_KEYWORD('UNIQUE') + ' (%s)' % \
            ", ".join([style.SQL_FIELD(qn(opts.get_field(f).column)) for f in field_constraints]))

    full_statement = [style.SQL_KEYWORD('CREATE TABLE IF NOT EXISTS') + ' ' + style.SQL_TABLE(qn(opts.db_table)) + ' (']
#    print 'table_output', '\n'.join(table_output)
    for i, line in enumerate(table_output): # Combine and add commas.
        full_statement.append('    %s%s' % (line, i < len(table_output)-1 and ',' or ''))
    full_statement.append(')')
    if opts.db_tablespace:
        full_statement.append(self.connection.ops.tablespace_sql(opts.db_tablespace))
    full_statement.append(';')
    final_output.append('\n'.join(full_statement))

    if opts.has_auto_field:
        # Add any extra SQL needed to support auto-incrementing primary keys.
        auto_column = opts.auto_field.db_column or opts.auto_field.name
        autoinc_sql = self.connection.ops.autoinc_sql(opts.db_table, auto_column)
        if autoinc_sql:
            for stmt in autoinc_sql:
                final_output.append(stmt)
    
#    print 'table_output_dict', table_output_dict
    for col, out in table_output_dict.items():        
        s = safe_add_column(qn, opts.db_table, col, ' '.join(out))
#        print s
#        final_output.append(s)
#        final_output.append('\n'.join(s))
        final_output.extend(s)

    return final_output, pending_references

import types

def sql_create(app, style, connection):
    "Returns a list of the CREATE TABLE SQL statements for the given app."

    print '--- custom sql_create ---'

    if connection.settings_dict['ENGINE'] == 'django.db.backends.dummy':
        # This must be the "dummy" database backend, which means the user
        # hasn't set ENGINE for the databse.
        raise CommandError("Django doesn't know which syntax to use for your SQL statements,\n" +
            "because you haven't specified the ENGINE setting for the database.\n" +
            "Edit your settings file and change DATBASES['default']['ENGINE'] to something like\n" +
            "'django.db.backends.postgresql' or 'django.db.backends.mysql'.")

    # Get installed models, so we generate REFERENCES right.
    # We trim models from the current app so that the sqlreset command does not
    # generate invalid SQL (leaving models out of known_models is harmless, so
    # we can be conservative).
    app_models = models.get_models(app, include_auto_created=True)
    final_output = []
    tables = connection.introspection.table_names()
    known_models = set([model for model in connection.introspection.installed_models(tables) if model not in app_models])
    pending_references = {}

    connection.creation.sql_create_model__old = connection.creation.sql_create_model
    connection.creation.sql_create_model = types.MethodType(sql_create_model__custom, connection.creation)

#    for model in app_models[0:1]:   
    for model in app_models:   
        output, references = connection.creation.sql_create_model(model, style, known_models)
#        print '\n'.join(output)
        final_output.extend(output)
        for refto, refs in references.items():
            pending_references.setdefault(refto, []).extend(refs)
            if refto in known_models:
                final_output.extend(connection.creation.sql_for_pending_references(refto, style, pending_references))
        final_output.extend(connection.creation.sql_for_pending_references(model, style, pending_references))
        # Keep track of the fact that we've created the table for this model.
        known_models.add(model)

    # Handle references to tables that are from other apps
    # but don't exist physically.
    not_installed_models = set(pending_references.keys())
    if not_installed_models:
        alter_sql = []
        for model in not_installed_models:
            alter_sql.extend(['-- ' + sql for sql in
                connection.creation.sql_for_pending_references(model, style, pending_references)])
        if alter_sql:
            final_output.append('-- The following references should be added but depend on non-existent tables:')
            final_output.extend(alter_sql)

    return final_output

def sql_custom(app, style, connection):
    "Returns a list of the custom table modifying SQL statements for the given app."
    output = []

    app_models = get_models(app)
    app_dir = os.path.normpath(os.path.join(os.path.dirname(app.__file__), 'sql'))

    for model in app_models:
        out = custom_sql_for_model(model, style, connection)
#        print out
        output.extend(out)

    return output
#
#def sql_indexes(app, style, connection):
#    "Returns a list of the CREATE INDEX SQL statements for all models in the given app."
#    output = []
#    for model in models.get_models(app):
#        output.extend(connection.creation.sql_indexes_for_model(model, style))
#    return output
#
#def sql_all(app, style, connection):
#    "Returns a list of CREATE TABLE SQL, initial-data inserts, and CREATE INDEX SQL for the given module."
#    return sql_create(app, style, connection) + sql_custom(app, style, connection) + sql_indexes(app, style, connection)
#

def custom_sql_for_model(model, style, connection):
#    print '--- custom custom_sql_for_model ---'
    opts = model._meta
    app_dir = os.path.normpath(os.path.join(os.path.dirname(models.get_app(model._meta.app_label).__file__), 'sql'))
    output = []

    # Post-creation SQL should come before any initial SQL data is loaded.
    # However, this should not be done for models that are unmanaged or
    # for fields that are part of a parent model (via model inheritance).
    if opts.managed:
        post_sql_fields = [f for f in opts.local_fields if hasattr(f, 'post_create_sql')]
        for f in post_sql_fields:
            output.extend(f.post_create_sql(style, model._meta.db_table))

    # Some backends can't execute more than one SQL statement at a time,
    # so split into separate statements.
    statements = re.compile(r";[ \t]*$", re.M)

    # Find custom SQL, if it's available.
    backend_name = connection.settings_dict['ENGINE'].split('.')[-1]
    sql_files = [os.path.join(app_dir, "%s.%s.sql" % (opts.object_name.lower(), backend_name)),
                 os.path.join(app_dir, "%s.sql" % opts.object_name.lower())]
    for sql_file in sql_files:
        if os.path.exists(sql_file):
            fp = open(sql_file, 'U')
            for statement in statements.split(fp.read().decode(settings.FILE_CHARSET)):
                # Remove any comments from the file
                statement = re.sub(ur"--.*([\n\Z]|$)", "", statement)
                if statement.strip():
                    output.append(statement + u";")
            fp.close()

    return output
#
#
#def emit_post_sync_signal(created_models, verbosity, interactive, db):
#    # Emit the post_sync signal for every application.
#    for app in models.get_apps():
#        app_name = app.__name__.split('.')[-2]
#        if verbosity >= 2:
#            print "Running post-sync handlers for application", app_name
#        models.signals.post_syncdb.send(sender=app, app=app,
#            created_models=created_models, verbosity=verbosity,
#            interactive=interactive, db=db)

custom_sql_for_model_old = custom_sql_for_model
def custom_sql_for_model(model, style, connection):
#    print 'custom_sql_for_model'
    return []


from django.core.management.commands.sqlall import *
