

import settings
from django.core.management import setup_environ
setup_environ(settings)

import types

from django.core.management.commands.syncdb import *
from altersql import sql_create_model__custom
from acid.utils import exc_str


class Command(NoArgsCommand):
#    option_list = NoArgsCommand.option_list + (
#        make_option('--noinput', action='store_false', dest='interactive', default=True,
#            help='Tells Django to NOT prompt the user for input of any kind.'),
#        make_option('--database', action='store', dest='database',
#            default=DEFAULT_DB_ALIAS, help='Nominates a database to synchronize. '
#                'Defaults to the "default" database.'),
#        make_option('--app', action='store', dest='application',
#            default=None, help='Select app if need'),
#        make_option('--showsql', action='store_true', dest='showsql',
#            default=False, help='Show sql queries'),
#    )
#    help = "Create the database tables for all apps in INSTALLED_APPS whose tables haven't already been created."

    def handle_noargs(self, **options):
        print 'exec sql...'

        from django.db import connection, transaction
        cursor = connection.cursor()


        try:
            while True:
                try:
                    q = raw_input()
                    try:
                        cursor.execute(q)
                    except:
                        print exc_str()
                    
                except KeyboardInterrupt:
#                    print "press ctrl+c %d times to force exit" % keyint_rem
                    pass
        
        except EOFError:
#            print 'pipe_log done reading'
            pass

        transaction.commit_unless_managed()
