import settings
from django.core.management import setup_environ
setup_environ(settings)

import types

from django.core.management.commands.syncdb import *
#from altersql import sql_create_model__custom
from acid.utils import exc_str

import sys
sys.modules["django.core.management.commands.syncdb"] = sys.modules["django_snippets.dropall"]


class Command(NoArgsCommand):
    option_list = NoArgsCommand.option_list + (
        make_option('--noinput', action='store_false', dest='interactive', default=True,
            help='Tells Django to NOT prompt the user for input of any kind.'),
        make_option('--database', action='store', dest='database',
            default=DEFAULT_DB_ALIAS, help='Nominates a database to synchronize. '
                'Defaults to the "default" database.'),
    )
    help = "Create the database tables for all apps in INSTALLED_APPS whose tables haven't already been created."

    def handle_noargs(self, **options):

        verbosity = int(options.get('verbosity', 1))
        interactive = options.get('interactive')
        show_traceback = options.get('traceback', False)

        # Stealth option -- 'load_initial_data' is used by the testing setup
        # process to disable initial fixture loading.
        load_initial_data = options.get('load_initial_data', True)

        self.style = no_style()

        # Import the 'management' module within each installed app, to register
        # dispatcher events.
        for app_name in settings.INSTALLED_APPS:
            try:
                import_module('.management', app_name)
            except ImportError, exc:
                # This is slightly hackish. We want to ignore ImportErrors
                # if the "management" module itself is missing -- but we don't
                # want to ignore the exception if the management module exists
                # but raises an ImportError for some reason. The only way we
                # can do this is to check the text of the exception. Note that
                # we're a bit broad in how we check the text, because different
                # Python implementations may not use the same text.
                # CPython uses the text "No module named management"
                # PyPy uses "No module named myproject.myapp.management"
                msg = exc.args[0]
                if not msg.startswith('No module named') or 'management' not in msg:
                    raise

        db = options.get('database', DEFAULT_DB_ALIAS)
        connection = connections[db]
        cursor = connection.cursor()

        # Get a list of already installed *models* so that references work right.
        tables = connection.introspection.table_names()
        seen_models = connection.introspection.installed_models(tables)
        created_models = set()
        pending_references = {}

        # Build the manifest of apps and models that are to be synchronized
        all_models = [
            (app.__name__.split('.')[-2],
                [m for m in models.get_models(app, include_auto_created=True)
                if router.allow_syncdb(db, m)])
            for app in models.get_apps()
        ]
        def model_installed(model):
            return True
#            opts = model._meta
#            converter = connection.introspection.table_name_converter
#            return not ((converter(opts.db_table) in tables) or
#                (opts.auto_created and converter(opts.auto_created._meta.db_table) in tables))

        manifest = SortedDict(
            (app_name, filter(model_installed, model_list))
            for app_name, model_list in all_models
        )

        # Create the tables for each model
        if verbosity >= 1:
            print "Dropping tables v2 ..."
            
        cursor.execute('SET foreign_key_checks = 0')
            
        for app_name, model_list in manifest.items():
            for model in model_list:
                # Create the model's database table, if it doesn't already exist.
                if verbosity >= 3:
                    print "Processing %s.%s model" % (app_name, model._meta.object_name)
                try:
                    cursor.execute('DROP TABLE %s' % (model._meta.db_table, ))
                except:
                    print "can't drop " + model._meta.db_table
                    raise


        transaction.commit_unless_managed(using=db)
